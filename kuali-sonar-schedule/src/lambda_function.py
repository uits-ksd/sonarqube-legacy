import json
import boto3

print('Loading function now')

# input determines if we start or stop SonarQube
def lambda_handler(event, context):
    
    # hardcoded variables.
    pSonarDBName = 'sonar-prd'
    pAutoScalingGroupName = 'kuali-sonar-base-asg'
    pDesiredCapacityCount = 1
    
    # setup RDS client
    rds_client = boto3.client('rds')
    rds_response = rds_client.describe_db_instances(DBInstanceIdentifier=pSonarDBName)
    rds_status = rds_response.get('DBInstances')[0].get('DBInstanceStatus')
    
    # setup ASG client
    asg_client = boto3.client('autoscaling') 
    asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[pAutoScalingGroupName])
    asg_desired_capacity = asg_response.get('AutoScalingGroups')[0].get('DesiredCapacity')

    
    print("")
    print("--->ACTION: " + event['action'])
    
    if event['action'] == 'start':
        # Startup database
        if rds_status == 'stopped':
            print(" Starting database instance " + str(pSonarDBName))
            rds_start_response = rds_client.start_db_instance(DBInstanceIdentifier=pSonarDBName)
            rds_start_status = rds_start_response.get('DBInstance').get('DBInstanceStatus')
            print(" Database instance " + str(pSonarDBName) + " status: " + str(rds_start_status))    
        # Update asg desired capacity.    
        if asg_desired_capacity == 0:
            print(" Asg group " + str(pAutoScalingGroupName) + " set desired capacity: " + str(pDesiredCapacityCount))
            asg_sdc_response = asg_client.set_desired_capacity(AutoScalingGroupName=pAutoScalingGroupName, DesiredCapacity=pDesiredCapacityCount) 
    
    if event['action'] == 'stop':
        # Shutdown database
        if rds_status == 'available':
            print(" Stopping database instance " + str(pSonarDBName) )
            rds_stop_response = rds_client.stop_db_instance(DBInstanceIdentifier=pSonarDBName)
            rds_stop_status = rds_stop_response.get('DBInstance').get('DBInstanceStatus')
            print(" Database instance " + str(pSonarDBName) + " status: " + str(rds_stop_status))
        #set container instances to 0.
        if asg_desired_capacity > 0:
            print(" Asg group " + str(pAutoScalingGroupName) + " set desired capacity: 0")
            asg_sdc_response = asg_client.set_desired_capacity(AutoScalingGroupName=pAutoScalingGroupName, DesiredCapacity=0) 
    print("")
    #return event['key1'] # Echo back the first key value
 
